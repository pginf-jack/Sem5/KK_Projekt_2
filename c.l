%{
#include <stdio.h> /* printf() */
#include <string.h> /* strcpy */
#include <stdlib.h> /* atoi(), atof() */
#include "common.h" /* MAX_STR_LEN */
#include "c.tab.h" /* deklaracja symboli końcowych */

/* wypisanie informacji o znalezionym symbolu końcowym */
int process_token(const char *text, const char *TokenType,
                  const char *TokenVal, const int TokenID); 

int comm_beg = 0; /* wiersz rozpoczęcia komentarza */
%}

/* deklaracja warunków początkowych analizatora */
%x NAPIS
%x KOMENTARZ

/* wlacz sledzenie numerow linii */
%option yylineno

%%

  /* B. Wykrywanie slow kluczowych */
char return process_token(yytext, "KW_CHAR", "", KW_CHAR);
do return process_token(yytext, "KW_DO", "", KW_DO);
double return process_token(yytext, "KW_DOUBLE", "", KW_DOUBLE);
else return process_token(yytext, "KW_ELSE", "", KW_ELSE);
float return process_token(yytext, "KW_FLOAT", "", KW_FLOAT);
for return process_token(yytext, "KW_FOR", "", KW_FOR);
if return process_token(yytext, "KW_IF", "", KW_IF);
int return process_token(yytext, "KW_INT", "", KW_INT);
long return process_token(yytext, "KW_LONG", "", KW_LONG);
short return process_token(yytext, "KW_SHORT", "", KW_SHORT);
struct return process_token(yytext, "KW_STRUCT", "", KW_STRUCT);
unsigned return process_token(yytext, "KW_UNSIGNED", "", KW_UNSIGNED);
void return process_token(yytext, "KW_VOID", "", KW_VOID);
while return process_token(yytext, "KW_WHILE", "", KW_WHILE);

  /* C. Usuwanie bialych znakow */
[[:space:]]+ ;

  /* D. Usuwanie komentarzy jednowierszowych */
\/\/.*$ ;

  /* E. Wykrywanie operatorow wieloznakowych */
"<=" return process_token(yytext, "LE", "", LE);
"++" return process_token(yytext, "INC", "", INC);

  /* F. Wykrywanie identyfikatorow */
[A-Za-z_][A-Za-z0-9_]* return process_token(yytext, "IDENT", yytext, IDENT);

  /* G. Wykrywanie liczb calkowitych i zmiennoprzecinkowych */
[0-9]+ return process_token(yytext, "INTEGER_CONST", yytext, INTEGER_CONST);
(([0-9]+\.[0-9]*)|([0-9]*\.[0-9]+))([eE](\+|-)?[0-9]{1,2})? {
  return process_token(yytext, "FLOAT_CONST", yytext, FLOAT_CONST);}

  /* H. Wykrywanie stalych tekstowych (napisow) */
\"[^\"\n]*\" return process_token(yytext, "STRING_CONST", yytext, STRING_CONST);

  /* I. Wykrywanie stalych znakowych */
'[^'\n]' return process_token(yytext, "CHARACTER_CONST", yytext, CHARACTER_CONST);

  /* J. Wykrywanie symboli koncowych jednoznakowych: operatorow, interpunkcji */
[+\-*/;,=(){}.[\]<>?:] return process_token(yytext, yytext, "", yytext[0]);

  /* K. Wykrywanie dyrektywy dolaczania plikow */
#include[ ](<[A-Za-z0-9_\-.]+>|\"[A-Za-z0-9_\-.]+\") {
  printf("Przetwarzanie dyrektywy %s\n", yytext);}

  /* L. Wykrywanie napisow z uzyciem warunkow poczatkowych */
<INITIAL>\" BEGIN(NAPIS);
<NAPIS>\" {
  BEGIN(INITIAL);
  return process_token(yytext, "STRING_CONST", yytext, STRING_CONST);}
<NAPIS>. yymore();
<NAPIS>\n {
  fprintf(stderr, "Niezamknięty napis otwarty w wierszu %d\n", yylineno-1);
  BEGIN(INITIAL);}

  /* M. Usuwanie komentarzy wielowierszowych z uzyciem warunkow poczatkowych */
<INITIAL>"/*" { BEGIN(KOMENTARZ); comm_beg = yylineno; } /* wejście w warunek */
<INITIAL>"*/" {
  fprintf(stderr, "Nieoczekiwane zamknięcie komentarza w wierszu %d\n", yylineno);} /* N. */
<KOMENTARZ>"*/" BEGIN(INITIAL); /* powrót */
<KOMENTARZ>.|\n /* pomiń wnętrze komentarza */

%%

/* Nazwa:       process_token
 * Cel:         Wypisanie informacji o wykrytym elemencie i przekazanie
 *              tej informacji wyżej z ewentualną wartością elementu, jeśli
 *              takowa występuje.
 * Parametry:   text            - (i) tekst, do którego nastąpiło dopasowanie;
 *              TokenType       - (i) napis będący tekstową reprezentacją
 *                                      nazwy typu elementu;
 *              TokenVal        - (i) wartość elementu, o ile występuje;
 *              TokenID         - (i) identyfikator typu elementu zadeklarowany
 *                                      za pomocą dyrektywy %token
 *                                      w pliku c.y lub kod pojedynczego
 *					znaku (w pliku analizatora składniowego
 *					występuje ujęty w apostrofy).
 * Zwraca:      Identyfikator typu elementu (TokenID).
 * Uwagi:       Informacja o wykrytym elemencie zwracana jest
 *              w trzech kolumnach. W pierwszej wypisywany jest tekst,
 *              do którego nastąpiło dopasowanie, w drugiej - typ elementu,
 *              w trzeciej - wartość elementu (o ile występuje).
 */
int process_token(const char *text, const char *TokenType,
                  const char *TokenVal, const int TokenID)
{
  int l;
  printf("%-20.20s%-15s %s\n", text, TokenType, TokenVal);
  switch (TokenID) {

  case INTEGER_CONST:
    yylval.i = atoi(text); break;

  case FLOAT_CONST:
    yylval.d = atof(text); break;

  case IDENT:
    strncpy(yylval.s, text, MAX_STR_LEN); break;

  case STRING_CONST:
    l = strlen(TokenVal);
    yylval.s[0] = '\0';
    strncpy(yylval.s, TokenVal + 1, l - 2 <= MAX_STR_LEN ? l - 1 : MAX_STR_LEN);
    break;

  case CHARACTER_CONST:
    yylval.i = text[1]; break;

  }
  return(TokenID);
}


int yywrap( void )
{ /* funkcja wywoływana po napotkaniu końca strumienia wejściowego

  /* sprawdzenie, czy warunek poczatkowy YY_START różni się od INITIAL. */
  /* Jeżeli tak, to oznacza to niezamknięty komentarz lub stała tekstową
     - wypisujemy informację o błędzie. */
  switch (YY_START) {
  case INITIAL:
    break;
  case KOMENTARZ:
    fprintf(stderr, "Brak zamknięcia komentarza otwartego w wierszu %d\n", comm_beg);
    break;
  case NAPIS:
    fprintf(stderr, "Niezamknięty napis otwarty w wierszu %d\n", yylineno-1);
    break;
  default:
    fprintf(stderr, "Osiagnięto kod nieosiagąlny\n");
  }
  return( 1 ); /* koniecznie, by analiza nie rozpoczęła się od nowa */
}


